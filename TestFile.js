function(instance, initialize, context){
    //testing if the url stay the same for the updated commit. Last url was : https://bitbucket.org/Jonathan_Lefebvre/testrepo/raw/746ad32adcbb456aed37509da2018e895bc53fc0/TestFile.js
    let NEWMODIF = "i've modified this";
    
	let htmlViewer = "<div id=forgeViewer></div>";
    //<section id=loadingBox><div><p id=loadingStatus></div></section>
    //The Viewer will be instantiated here
    instance.canvas.append(htmlViewer);
    
    //Function declarations here
    
    function updateStatus(viewerAccessToken, viewerUrn){
        switch(instance.data.status){
            case 'success':
                $("#loadingBox").css({"display": "none"});
                instance.data.showViewer(viewerAccessToken, viewerUrn);
                break;
            case 'failed':
                window.alert('Error: getManifest() status is failed.!\nIt looks like there was a problem with your request. Please try the following steps:\n» The uploaded file type may not be supported. Make sure the uploaded file is a DWG file. \n» Your file may be corrupted, please try a new one.\n» If the issue still persists, please report it to your system administrator.');
                break;
            case 'timeout':
                window.alert('Error: getManifest() status is timeout.!\nIt looks like there was a problem with your request.\nThere might be an issue with your network. Please refresh the page and try again.\n» There may be an issue with the Autodesk Forge Viewer API server. If the issue still persists after a few moments, please report it to your system administrator.');
                break;
            default:
                $("#loadingStatus").html("Error: " + instance.data.status + ". There was a problem fetching manifest. Please contact your system administrator");
                break;
        }
    }
    
	//Handles the API Call Get Manifest
    
    instance.data.getStatus = function getStatus(viewerAccessToken, viewerUrn){
        let settings = {
            "url": "https://developer.api.autodesk.com/modelderivative/v2/designdata/" + viewerUrn + "/manifest",
            "method": "GET",
            "timeout": 3600,
            "headers": {
                "Authorization": "Bearer " + viewerAccessToken,
            },
        };
        $.ajax(settings).done(function (response) {
            let status = response.status;
            $("#loadingStatus").html("Your document will be loaded soon, this could take a few minutes.<br>Status: " + status + '.');
            instance.data.status = status;
            if(status == 'pending' || status == 'inprogress'){
                setTimeout(getStatus, 3000, viewerAccessToken, viewerUrn);
            }
            else{
                updateStatus(viewerAccessToken, viewerUrn);
            }
        });
        // If the request fails, saves the error code to the instance.data.status & redirects to updateStatus for error management
        $(document).ajaxError(function(event, xhr, options, exc){
            instance.data.status = xhr.status;
            updateStatus(viewerAccessToken, viewerUrn);
        });
    }
    
    //Autodesk Viewer Code

    instance.data.showViewer = function showViewer(viewerAccessToken, viewerUrn){
        var viewer;
        var options = {
            //env: 'AutodeskProduction',
            //api: 'derivativeV2',
            //getAccessToken: function(onTokenReady) {
                //var token = viewerAccessToken;
                //var timeInSeconds = 3600;
                //onTokenReady(token, timeInSeconds);
            //}
            env: "Local",
            useADP: false
        };
        /*Autodesk.Viewing.Initializer(options, function() {
            //let config = {
                //Registers the Extension
               // extensions: ['Autodesk.CustomDocumentBrowser']
            //}
            var pdf = 'urn:adsk.objects:os.object:jonahtestbucket_123/testpdf.pdf';
            var htmlDiv = document.getElementById('forgeViewer');
            viewer = new Autodesk.Viewing.GuiViewer3D(htmlDiv);
            var startedCode = viewer.start();
            viewer.setTheme("light-theme");
            viewer.loadExtension('Autodesk.PDF').then( () => {
                viewer.loadModel( pdf, {page: 1} onDocumentLoadSuccess, onDocumentLoadFailure);
            });*/
        let viewer;

        function initializeViewer( pdf ) {
            var options = {
                env: "Local",
                useADP: false
            }
            Autodesk.Viewing.Initializer(options, () => {
                viewer = new Autodesk.Viewing.Private.GuiViewer3D(document.getElementById('forgeViewer'));
                viewer.setTheme("light-theme");
                viewer.start();
                if (!pdf) return;
                viewer.loadExtension('Autodesk.PDF').then( () => {
                    viewer.loadModel( pdf , viewer);
                    viewer.loadExtension("Autodesk.Viewing.MarkupsCore")
                    viewer.loadExtension("Autodesk.Viewing.MarkupsGui")
                });
            });
            
            if (startedCode > 0) {
                console.error('Failed to create a Viewer: WebGL not supported.');
                $("#loadingStatus").html("Failed to create a Viewer: WebGL not supported.");
                return;
            }
            console.log('Initialization complete, loading a model next...');
        });
        //let documentId = `urn:` + viewerUrn;
        Autodesk.Viewing.Document.load(/*documentId*/onDocumentLoadSuccess, onDocumentLoadFailure);
        function onDocumentLoadSuccess(viewerDocument) {
            var defaultModel = viewerDocument.getRoot().getDefaultGeometry();
            viewer.loadDocumentNode(viewerDocument, defaultModel);
        }
        function onDocumentLoadFailure() {
            console.error('Failed fetching Forge manifest');
            $("#loadingStatus").html("Failed fetching Forge manifest.");
        }
    }
}
